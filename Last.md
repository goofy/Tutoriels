# LAST : rassembler ses meilleurs Pouets et les publier sur une page web
## Le guide du parfait débutant


**Goofy** _LAST ? Encore un truc de geek ?_

**Luc** Last, c'est un système de publication en page web des Pouets que tu as commis sur le réseau social Mastodon. 

**Goofy** _Ah ça m'amuserait assez oui d'avoir mes pages web avec mes bricoles genre #ironèmes ou #MercrediFiction. Comment ça marche ton truc ?_

**Luc** Last repose sur deux ressources :

- un dépôt GitLab ;
- les _Gitlab Pages_

Le principe est simple : tu alimentes ton dépôt en lui donnant l'adresse de tes Pouets, et grâce à mon script génial ils sont pris en charge par les _GitLab Pages_ qui te les propose proprement disposés sur une page HTML rien qu'à toi, sans que tu aies besoin de lever le petit doigt pour ça. C'est ça la beauté du code : automatiser les trucs pénibles.

**Goofy** _Ouh là mais comment je vais faire ça moi, je n'y connais rien en informatique, je ne veux pas me mettre à coder pour réunir mes textes et d'abord comment je fais pour avoir un dépôt Git ?_
<!-- hum ça m'a l'air trop compliqué de se crée un dépot git pour un⋅e vraie⋅e débutant⋅e autant dire de cloner ton repo ?-->
**Luc** eh bien tu vas créer un compte [sur cette page](https://framagit.org/users/sign_in)
en choisissant **Register**.

**Goofy** _hum bon je remplis ça… et puis encore ça… bon finalement ça va vite et ils ne me demandent pas la taille de mes chaussettes…_

**Luc** maintenant tu vas simplement copier (c'est permis et même recommandé, t'es plus à l'école hein) et importer le contenu de mon dépôt vers le tien. 
Tu vois [ma page consacrée à Last dans le framagit](https://framagit.org/luc/last)? Eb bien tu vas repérer le bouton de fork et cliquer dessus. [ici screenshot]

**Goofy** _Je crois que ça a marché, mon dépôt Git est rempli de trucs bizarres ! T'es sûr que c'est pas des virus russes qui vont me réclamer des bitcoins ?_

**Luc** (à part) quel boulet, dans quelle galère me suis-je mis ? (à Goofy) meuh non puisque mon pipeline est tout vert !

**Goofy** _admettons, il est ouvert (je me demande… macho comme il est, c'est comme ça qu'il appelle sa zigounette je parie…)_ Et maintenant, qu'est-ce que je fais ? Faut cliquer quelque part ? Tiens sur ce beau bouton vert là

**Luc** SttooOP ! Maintenant il ne te reste plus qu'à personnaliser ta future page web. pour cela, tu vas aller dans le fichier de configuration qui s'appelle `last.conf`, tu l'as repéré ?

**Goofy** _oui mais euh ça veut dire quoi, "tu vas aller dans le fichier", comment je fais moi pour aller dans un fichier ?_

**Luc** (soupire et boit une gorgée de bière) eh bien tu vas le modifier en utilisant le bouton EDIT en haut à droite qui va te permetter d'écrire dedans (oui avec tes 4 doigts sur le clavier).

**Goofy** _voilà. Ah voui je peux écrire, Tiens je vais essayer d'écrire une ode poétique au PHP… « Ô toi langage des dieux que seuls utilisent…»_

**Luc** Rhhaaa (il boit une gorgée de bière) mais arrrrêttteuh ! Tu vas à la ligne [TODO Luc] et tu remplaces zzz par ggg
etc. 
[TODO Luc]
l'adresse des pouets, le titre et le tag etc.

**Goofy** _ça y est je crois que c'est bon. Je ne vois pas comment tout ça va faire une page web_

**Luc** blabalab regarde dans le fichier truc, la structure de la page web blablabla va chercher les feuilles de style pour avoir une jolie mise en page , puis blablabal

**Goofy** _Ah ok ! Et maintenant , elle est où la page web ?_

**Luc** hahaha mais elle est déjà publiée ici qkjdhfqlkjhfqlk

**Goofy** _ah mais c'est de la magie ton truc, c'est vraiment épatant. Mais qu'est-ce que c'est ce lien `epub` là ?_

**Luc** (reprend une grande goulée de bière pour fêter ça) Eh bien vois-tu, si tu veux publier tes œuvres impérissables sous forme de livre électronique, tu les obtiendras ainsi sous ce format qui justement permet d'y accéder avec une liseuse.

**Goofy** _alors là mon cher Luc vous m'épatâtes ! Mais comment je fais pour ajouter mes nouveaux pouets pouétiques sur cette page ?_

**Luc** Très simple : comme tu l'as déjà fait la première fois, tu vas sur ton dépôt, tu ouvres le fichier de configuration et tu ajoutes une ligne avec l'adresse du nouveau pouet, en veillant bien à la mettre entre apostrophes et à terminer la ligne par une virgule

**Goofy** _Je crois que j'ai compris. Et si je veux modifier un peu la présentation visuelle de la page, lui donner un style un peu plus fun ?_

**Luc** Il suffit d'aller modifier un autre fichier, celui qui donne son style à la page. Tu le trouveras dans xxxx. Si par exemple tu veux que ton texte apparaisse en vert sur fond noir, comme dans les films avec les méchants hackers, tu fais [todo Luc ]
